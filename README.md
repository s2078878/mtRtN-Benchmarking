# mtRtN-Benchmarking - mtDNA Variant Calling Benchmarking

This Nextflow pipeline will run all pre-call benchmarking conducted for the mtDNA variant calling pipelines. 

## Results 
This pipeline will take several hours to run and will output a html document containing the results, this has been supplied above to avoid unnecessarily running the pipeline. If you would still like to run the pipeline, follow the steps below.

## Running the Pipeline

### 1.Cloning Repository

Clone the repository and cd into the directory using the following commands:

```shell
git clone https://git.ecdf.ed.ac.uk/s2078878/mtRtN-Benchmarking.git
cd mtRtN-Benchmarking
```

### 2.Create Environment

To create a conda environment with all the necessary packages, run the following command:

```shell
conda env create --file env.yml
```

Then activate the environment
```shell
conda activate mtRtN-Benchmarking
```

### 3.Run mtRtN-Benchmarking

The pipeline should now be ready to run. All benchmarking data will be automatically downloaded, reference files indexed, and output files generated, there is no need to supply any additional parameters. The command to run the pipeline is supplied below.

```shell
nextflow run mtRtN-Benchmarking.nf
```
