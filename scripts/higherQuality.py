#!/usr/bin/env python

#Load modules
import pysam
import sys

bamfile = sys.argv[1]
outfile = sys.argv[2]

#Load bam
samfile = pysam.AlignmentFile(bamfile, "rb")

# open the outfile ready to append clusters to
output = open(outfile,'w')

# Loop through autosome bam file and find corresponding read in mitochondria file
for read in samfile.fetch():
    name = read.query_name
    direc = "R1" if read.is_read1 else "R2"
    qual = read.mapping_quality

    output.write("{}\t{}\t{}\n".format(name, direc, qual))

# close output file and samfile
output.close()
samfile.close()