#!/bin/bash
######################################################################################
# This script will download all synthetic data required for running the benchmarking #
######################################################################################

## M1 (50% VAF)
wget https://zenodo.org/record/4001424/files/M1-Clontech_S6.bam 
wget https://zenodo.org/record/4001424/files/M1-Clontech_S6.bam.bai 
wget https://zenodo.org/record/4001424/files/M1-Herk_S1.bam 
wget https://zenodo.org/record/4001424/files/M1-Herk_S1.bam.bai 
wget https://zenodo.org/record/4001424/files/M1-NEB_S21.bam 
wget https://zenodo.org/record/4001424/files/M1-NEB_S21.bam.bai 
wget https://zenodo.org/record/4001424/files/M1-PCR-Clontech_S16.bam 
wget https://zenodo.org/record/4001424/files/M1-PCR-Clontech_S16.bam.bai 
wget https://zenodo.org/record/4001424/files/M1-PCR-Herk_S11.bam 
wget https://zenodo.org/record/4001424/files/M1-PCR-Herk_S11.bam.bai 
wget https://zenodo.org/record/4001424/files/M1-PCR-NEB_S26.bam 
wget https://zenodo.org/record/4001424/files/M1-PCR-NEB_S26.bam.bai 

## M2 (10% VAF)
wget https://zenodo.org/record/4001424/files/M2-Clontech_S7.bam 
wget https://zenodo.org/record/4001424/files/M2-Clontech_S7.bam.bai 
wget https://zenodo.org/record/4001424/files/M2-Herk_S2.bam 
wget https://zenodo.org/record/4001424/files/M2-Herk_S2.bam.bai 
wget https://zenodo.org/record/4001424/files/M2-NEB_S22.bam 
wget https://zenodo.org/record/4001424/files/M2-NEB_S22.bam.bai 
wget https://zenodo.org/record/4001424/files/M2-PCR-Clontech_S17.bam 
wget https://zenodo.org/record/4001424/files/M2-PCR-Clontech_S17.bam.bai 
wget https://zenodo.org/record/4001424/files/M2-PCR-Herk_S12.bam 
wget https://zenodo.org/record/4001424/files/M2-PCR-Herk_S12.bam.bai 
wget https://zenodo.org/record/4001424/files/M2-PCR-NEB_S27.bam 
wget https://zenodo.org/record/4001424/files/M2-PCR-NEB_S27.bam.bai 

## M3 (2% VAF)
wget https://zenodo.org/record/4001424/files/M3-Clontech_S8.bam 
wget https://zenodo.org/record/4001424/files/M3-Clontech_S8.bam.bai 
wget https://zenodo.org/record/4001424/files/M3-Herk_S3.bam 
wget https://zenodo.org/record/4001424/files/M3-Herk_S3.bam.bai 
wget https://zenodo.org/record/4001424/files/M3-NEB_S48.bam 
wget https://zenodo.org/record/4001424/files/M3-NEB_S48.bam.bai 
wget https://zenodo.org/record/4001424/files/M3-PCR-Clontech_S18.bam 
wget https://zenodo.org/record/4001424/files/M3-PCR-Clontech_S18.bam.bai 
wget https://zenodo.org/record/4001424/files/M3-PCR-Herk_S13.bam 
wget https://zenodo.org/record/4001424/files/M3-PCR-Herk_S13.bam.bai 
wget https://zenodo.org/record/4001424/files/M3-PCR-NEB_S28.bam 
wget https://zenodo.org/record/4001424/files/M3-PCR-NEB_S28.bam.bai 

## M4 (1% VAF)
wget https://zenodo.org/record/4001424/files/M4-Clontech_S9.bam 
wget https://zenodo.org/record/4001424/files/M4-Clontech_S9.bam.bai 
wget https://zenodo.org/record/4001424/files/M4-Herk_S4.bam 
wget https://zenodo.org/record/4001424/files/M4-Herk_S4.bam.bai 
wget https://zenodo.org/record/4001424/files/M4-NEB_S49.bam 
wget https://zenodo.org/record/4001424/files/M4-NEB_S49.bam.bai 
wget https://zenodo.org/record/4001424/files/M4-PCR-Clontech_S19.bam 
wget https://zenodo.org/record/4001424/files/M4-PCR-Clontech_S19.bam.bai 
wget https://zenodo.org/record/4001424/files/M4-PCR-Herk_S14.bam 
wget https://zenodo.org/record/4001424/files/M4-PCR-Herk_S14.bam.bai 
wget https://zenodo.org/record/4001424/files/M4-PCR-NEB_S29.bam 
wget https://zenodo.org/record/4001424/files/M4-PCR-NEB_S29.bam.bai 

## M5 (0.5% VAF)
wget https://zenodo.org/record/4001424/files/M5-Clontech_S10.bam 
wget https://zenodo.org/record/4001424/files/M5-Clontech_S10.bam.bai 
wget https://zenodo.org/record/4001424/files/M5-Herk_S5.bam 
wget https://zenodo.org/record/4001424/files/M5-Herk_S5.bam.bai 
wget https://zenodo.org/record/4001424/files/M5-NEB_S25.bam 
wget https://zenodo.org/record/4001424/files/M5-NEB_S25.bam.bai 
wget https://zenodo.org/record/4001424/files/M5-PCR-Clontech_S20.bam 
wget https://zenodo.org/record/4001424/files/M5-PCR-Clontech_S20.bam.bai 
wget https://zenodo.org/record/4001424/files/M5-PCR-Herk_S15.bam 
wget https://zenodo.org/record/4001424/files/M5-PCR-Herk_S15.bam.bai 
wget https://zenodo.org/record/4001424/files/M5-PCR-NEB_S30.bam 
wget https://zenodo.org/record/4001424/files/M5-PCR-NEB_S30.bam.bai 