#!/usr/bin/env nextflow

log.info """\
         =================================================================================
         M T B E N C H M A R K :   V A R I A N T   C A L L I N G   B E N C H M A R K I N G
         =================================================================================
         current_directory:     $baseDir
         out_directory:         $baseDir
         =================================================================================
         """
         .stripIndent()

/*
 * Extract only the mitochondria reads from the bam files
 */
process download {
    tag "Download synthetic data"

    output:
    path("*.{bam,bam.bai}") into downloaded_ch

    script:
    """
    cp $baseDir/scripts/downloadSynthetic.sh .
    bash downloadSynthetic.sh
    """
}

downloaded_ch
    .flatten()
    .map { file -> tuple(file.simpleName, file)}
    .groupTuple(by: 0)
    .map { sampleID, clust -> tuple( sampleID, clust.sort{it.name} ) }
    .map { input -> tuple(input[0], input[1][0], input[1][1]) }
    .set { file_ch }

/*
 * Create index files for alignment
 */
process index {
    tag "Creating index files"

    output:
    path("RtN/Calabrese_Dayama_Smart_Numts.*") into numt_ch
    path("RtN/humans.*") into humans_ch
    path("RtN/Nix_binary/rtn") into binary_ch

    script:
    """
    git clone https://github.com/Ahhgust/RtN.git
    bunzip2 RtN/humans.fa.bz2 && bwa index RtN/humans.fa
    """
}

/*
 * Create index files for alignment
 */
process reference {
    tag "Creating reference index files"

    output:
    path("index.*") into index_ch

    script:
    """
    wget 'ftp://hgdownload.cse.ucsc.edu/goldenPath/hg38/chromosomes/*.fa.gz'
    rm chrM.fa.gz
    gunzip *.fa.gz
    cat *.fa > hg38.fa

    bwa index -p index -a bwtsw hg38.fa
    """
}

/*
 * Extract only the mitochondria reads from the bam files
 */
process rcrs {
    tag "Pulling chrM from bam file"

    input:
    set sampleID, path(bam), path(index) from file_ch

    output:
    set sampleID, path("${sampleID}.mito.bam"), path("${sampleID}.mito.bam.bai")into mitochondria_ch, mitochondria_ch2
    set sampleID, path("${sampleID}.mito.bam") into mitochondria_ch3, mitochondria_ch4, mitochondria_ch5
    set sampleID, val("noFilt"), path("${sampleID}.mito.bam") into noFilt_ch
    path("${sampleID}.mito.bam") into noFilt_ch2
    path("${sampleID}.none_counts.txt") into noneCounts_ch

    script:
    """
    samtools view -hb ${bam} rCRS > ${sampleID}.mito.bam
    samtools index ${sampleID}.mito.bam

    samtools view -c ${sampleID}.mito.bam > ${sampleID}.none_counts.txt
    """
}


/*
 * Subset the BAM file to the target regions and output reads
 */
process subset {
    tag "Subsetting to fastq files"

    input:
    set sampleID, path(bam), path(index) from mitochondria_ch2

    output:
    set sampleID, path("${sampleID}.R1.fastq.gz"), path("${sampleID}.R2.fastq.gz") into subset_ch

    script:
    """
    cp $baseDir/data/rCRS.bed .
    bazam -bam ${bam} -L rCRS.bed -pad 0 -r1 ${sampleID}.R1.fastq -r2 ${sampleID}.R2.fastq 
    gzip *.fastq
    """
}

/*
 * Realign the extracted fastq files back to the autosomes
 */
process align {
    tag "Realigning reads"

    input:
    set sampleID, path(read1), path(read2) from subset_ch
    path(index) from index_ch

    output:
    set sampleID, path("${sampleID}.auto.bam") into realigned_ch, realigned_ch2
    
    script:
    """
    bwa mem index ${read1} ${read2} | samtools view -hb - > ${sampleID}.bam 
    samtools sort ${sampleID}.bam > ${sampleID}.auto.bam
    """
}

//////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////// R T N //////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
/*
 * Run RtN on chrM bam file to remove NUMT reads
 */
process rtn {
    tag "Removing NUMT reads"

    input:
    set sampleID, path(chrM_bam), path(chrM_index) from mitochondria_ch
    path(executable) from binary_ch
    path(reference) from humans_ch
    path(numts) from numt_ch

    output:
    set sampleID, val("rtn"), path("${sampleID}.mito.rtn.bam") into rtn_ch
    set sampleID, path("${sampleID}.mito.rtn.bam"), path("${sampleID}.mito.rtn.bam.bai") into rtn_ch2
    path("${sampleID}.mito.rtn.bam") into rtn_ch3
    path("${sampleID}.rtn_counts.txt") into rtnCounts_ch

    script:
    """
    ./rtn -p -h humans.fa -n Calabrese_Dayama_Smart_Numts.fa -b ${chrM_bam} -c rCRS
    samtools index ${sampleID}.mito.rtn.bam
    samtools view -c ${sampleID}.mito.rtn.bam > ${sampleID}.rtn_counts.txt
    """
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////// S H I F T   R E F E R E N C E //////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 * Create index files for alignment
 */
process shifted_reference {
    tag "Creating shifted reference index files"

    output:
    path("index.*") into shifted_index_ch

    script:
    """
    bwa index -p index -a bwtsw $baseDir/data/rCRS_Shifted.fa
    """
}

/*
 * Subset the BAM file to the target regions and output reads
 */
process subset_rtn {
    tag "Subsetting to fastq files"

    input:
    set sampleID, path(bam), path(index) from rtn_ch2

    output:
    set sampleID, path("${sampleID}.R1.fastq.gz"), path("${sampleID}.R2.fastq.gz") into subset_ch2

    script:
    """
    cp $baseDir/data/rCRS.bed .
    bazam -bam ${bam} -L rCRS.bed -pad 0 -r1 ${sampleID}.R1.fastq -r2 ${sampleID}.R2.fastq 
    gzip *.fastq
    """
}

/*
 * Realign the extracted fastq files back to the autosomes
 */
process realign {
    tag "Realigning reads to shifted reference"

    input:
    set sampleID, path(read1), path(read2) from subset_ch2
    path(index) from shifted_index_ch

    output:
    set sampleID, path("${sampleID}.shifted.bam") into shifted_ch
    
    script:
    """
    bwa mem index ${read1} ${read2} | samtools view -hb - > ${sampleID}.bam 
    samtools sort ${sampleID}.bam > ${sampleID}.shifted.bam
    """
}

/*
 * Sort the files and create mpileup file ready for varscan
 */
process pileup_shifted {
    tag "Sorting and creating pileup files"

    input:
    set sampleID, path(filtered_chrM) from shifted_ch

    output:
    set sampleID, path("${sampleID}.shifted.pileup") into shifted_mpileup_ch

    script:
    """
    samtools sort ${filtered_chrM} | \
    samtools mpileup -d 0 -f $baseDir/data/rCRS_Shifted.fa - > ${sampleID}.shifted.pileup
    """
}

/*
 * Run VarScan2 to call mitochondrial DNA variants and if they are germline/somatic
 */
process varscan_shifted {
    tag "Running VarScan2 on Shifted BAM"

    input:
    set sampleID, path(pileup) from shifted_mpileup_ch

    output:
    path("${sampleID}.shifted.Q30.snp") into shifted_varscan_ch

    script:
    """
    varscan mpileup2snp ${pileup} \
        --strand-filter 1 --min-avg-qual 30 \
        --min-coverage 2 --min-reads 2 --min-var-freq 0 > ${sampleID}.shifted.Q30.snp
    """
}

/*
 * Merge all variant calls
 */
process merge_shifted_varscan {
    tag "Merging Shifted VarScan Results"

    input:
    path(var_calls) from shifted_varscan_ch.collect()

    output:
    path("shifted_varscan.txt") into shifted_varscan_merged_ch

    script:
    """
    cp $baseDir/scripts/mergeCalls.R .
    Rscript mergeCalls.R ${var_calls} shifted_varscan.txt
    """
}


////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////// M A P Q //////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
/*
 * Merge bam files into single channel
 */
realigned_ch
    .mix(mitochondria_ch3)
    .groupTuple(by:0) 
    .map { sampleID, clust -> tuple( sampleID, clust.sort{it.name} ) }
    .map { input -> tuple(input[0], input[1][0], input[1][1]) }
    .set { merged_ch }

/*
 * Get all read IDs and their mapping quality
 */
process ids {
    tag "Getting IDs and mapping qualities"

    input:
    set sampleID, path(auto_bam), path(mito_bam) from merged_ch

    output:
    set sampleID, path("${sampleID}.mito_ids.txt"), path("${sampleID}.auto_ids.txt") into ids_ch

    script:
    """
    samtools index ${mito_bam}
    samtools index ${auto_bam}

    cp $baseDir/scripts/higherQuality.py .
    python higherQuality.py ${mito_bam} ${sampleID}.mito_ids.txt
    python higherQuality.py ${auto_bam} ${sampleID}.auto_ids.txt
    """
}

/*
 * Filter the reads for those with higher quality
 */
process filter_reads {
    tag "Identifying NUMT reads"

    input:
    set sampleID, path(mito_ids), path(auto_ids) from ids_ch

    output:
    set sampleID, path("${sampleID}.txt") into filtIDs_ch

    script:
    """
    cp $baseDir/scripts/compareQuality.R .
    Rscript compareQuality.R ${auto_ids} ${mito_ids} ${sampleID}.txt
    """
}

/*
 * Merge original bam file and ids to be removed into single channel
 */
filtIDs_ch
    .mix(mitochondria_ch4)
    .groupTuple(by:0) 
    .map { sampleID, clust -> tuple( sampleID, clust.sort{it.name} ) }
    .map { input -> tuple(input[0], input[1][0], input[1][1]) }
    .set { remove_ch }

/*
 * Filter the original mitochondria file 
 */
process filter_bam {
    tag "Identifying NUMT reads"

    input:
    set sampleID, path(bam_file), path(remove_ids) from remove_ch

    output:
    set sampleID, val("mapq"), path("${sampleID}.mapq.bam") into mapq_ch
    path("${sampleID}.mapq.bam") into mapq_ch2
    path("${sampleID}.mapq_counts.txt") into mapqCounts_ch

    script:
    """
    picard FilterSamReads I=${bam_file} O=${sampleID}.mapq.bam \
        READ_LIST_FILE=${remove_ids} FILTER=excludeReadList

    samtools view -c ${sampleID}.mapq.bam > ${sampleID}.mapq_counts.txt
    """
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////// N O T   E X C L U S I V E //////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 * Get list of read ids that can map to the autosomes
 */
process get_numt_reads {
    tag "Identifying NUMT reads"

    input:
    set sampleID, path(realigned_bam) from realigned_ch2

    output:
    set sampleID, path("${sampleID}.ids.txt") into ids_ch2

    script:
    """
    samtools view -F 4 -q 1 ${realigned_bam} | cut -f1 > ${sampleID}.ids.txt
    """
}

mitochondria_ch5
    .mix(ids_ch2)
    .groupTuple(by:0)
    .map { sampleID, clust -> tuple( sampleID, clust.sort{it.name} ) }
    .map { input -> tuple(input[0], input[1][0], input[1][1])}
    .set { merged_ch2 }

/*
 * Remove NUMT Reads
 */
process remove_numts {
    tag "Removing all NUMT reads"

    input:
    set sampleID, path(ids), path(chrM) from merged_ch2

    output:
    set sampleID, val("all"), path("${sampleID}.all.bam") into all_ch
    path("${sampleID}.all.bam") into all_ch2
    path("${sampleID}.all_counts.txt") into allCounts_ch

    script:
    """
    picard FilterSamReads I=${chrM} O=${sampleID}.all.bam \
        READ_LIST_FILE=${ids} FILTER=excludeReadList

    samtools view -c ${sampleID}.all.bam > ${sampleID}.all_counts.txt
    """
}


base_qualities = [0, 10, 20, 30, 40]

//////////////////
//// VarScan2 ////
//////////////////
rtn_ch
    .mix(mapq_ch)
    .mix(all_ch)
    .mix(noFilt_ch)
    .into{ merged_ch3; merged_ch5; merged_ch6; merged_ch7; merged_ch8 }

/*
 * Sort the files and create mpileup file ready for varscan
 */
process pileup {
    tag "Sorting and creating pileup files"

    input:
    set sampleID, method, path(filtered_chrM) from merged_ch3

    output:
    set sampleID, method, path("${sampleID}.${method}.pileup") into mpileup_ch

    script:
    """
    samtools sort ${filtered_chrM} | \
    samtools mpileup -d 0 -f $baseDir/data/rCRS.fa - > ${sampleID}.${method}.pileup
    """
}

/*
 * Run VarScan2 to call mitochondrial DNA variants and if they are germline/somatic
 */
process varscan {
    tag "Running VarScan2"

    input:
    set sampleID, method, path(pileup) from mpileup_ch
    each quality from base_qualities

    output:
    path("${sampleID}.${method}.Q${quality}.snp") into varscan_ch

    script:
    """
    varscan mpileup2snp ${pileup} \
        --strand-filter 1 --min-avg-qual ${quality} \
        --min-coverage 2 --min-reads 2 --min-var-freq 0 > ${sampleID}.${method}.Q${quality}.snp
    """
}

/*
 * Merge all variant calls
 */
process merge_varscan {
    tag "Merging VarScan results"

    input:
    path(var_calls) from varscan_ch.collect()

    output:
    path("varscan.txt") into varscan_merged_ch

    script:
    """
    cp $baseDir/scripts/mergeCalls.R .
    Rscript mergeCalls.R ${var_calls} varscan.txt
    """
}

///////////////////
//// Mutserve ////
//////////////////
rtn_ch3
    .mix(mapq_ch2)
    .mix(all_ch2)
    .mix(noFilt_ch2)
    .set{ merged_ch4 }

/*
 * Run mutserve 
 */
process mutserve {
    tag "Running Mutserve"

    input:
    path(bams) from merged_ch4.collect()
    each quality from base_qualities

    output:
    path("Q${quality}.txt") into mutserve_ch

    script:
    """
    cp $baseDir/data/rCRS.fa .
    curl -sL mutserve.vercel.app | bash
    ls *.bam | parallel samtools index '{}'
    ./mutserve call --reference rCRS.fasta --baseQ ${quality} --level 0 --output Q${quality} ${bams}
    """
}

/*
 * Merge mutserve results
 */
process merge_mutserve {
    tag "Merging Mutserve results"

    input:
    path(results) from mutserve_ch.collect()

    output:
    path("mutserve.txt") into mutserve_merged_ch

    script:
    """
    cp $baseDir/scripts/mergeMutserve.R .
    Rscript mergeMutserve.R ${results}
    """
}

///////////////
// Mutect2 ////
///////////////
process mutect2 {
    tag "Running Mutect2"

    input:
    set sampleID, method, path(filtered_chrM) from merged_ch5
    each quality from base_qualities

    output:
    path("${sampleID}.${method}.Q${quality}.vcf.gz") into mutect_ch

    script:
    """
    samtools index ${filtered_chrM}
    cp $baseDir/data/rCRS* .
    gatk Mutect2 -R rCRS.fa -L rCRS \
        --mitochondria-mode true \
        --min-base-quality-score ${quality} \
        -I ${filtered_chrM} \
        -O intermediate.vcf.gz

    gatk FilterMutectCalls \
        -V intermediate.vcf.gz \
        -O ${sampleID}.${method}.Q${quality}.vcf.gz \
        --mitochondria-mode true \
        -R rCRS.fa
    """
}

process merge_mutect2 {
    tag "Merging Mutect2 VCFs"

    input:
    path(vcfs) from mutect_ch.collect()

    output:
    path("mutect.txt") into mutect_merged_ch

    script:
    """
    cp $baseDir/scripts/mergeVCFs.R . 
    Rscript mergeVCFs.R ${vcfs} mutect.txt
    """
}

///////////////////
//// FreeBayes ////
///////////////////
process freebayes {
    tag "Running FreeBayes"

    input:
    set sampleID, method, path(filtered_chrM) from merged_ch6
    each quality from base_qualities

    output:
    path("${sampleID}.${method}.Q${quality}.vcf") into freebayes_ch

    script:
    """
    cp $baseDir/data/rCRS.fa .
    freebayes -f rCRS.fa --min-base-quality ${quality} ${filtered_chrM} > ${sampleID}.${method}.Q${quality}.vcf
    """
}

process merge_freebayes {
    tag "Merging FreeBayes VCFs"

    input:
    path(vcfs) from freebayes_ch.collect()

    output:
    path("freebayes.txt") into freebayes_merged_ch

    script:
    """
    cp $baseDir/scripts/mergeVCFs.R . 
    Rscript mergeVCFs.R ${vcfs} freebayes.txt
    """
}

////////////////
//// LoFreq ////
////////////////
process lofreq {
    tag "Running LoFreq"

    input:
    set sampleID, method, path(filtered_chrM) from merged_ch7
    each quality from base_qualities

    output:
    path("${sampleID}.${method}.Q${quality}.vcf") into lofreq_ch

    script:
    """
    cp $baseDir/data/rCRS.fa .
    lofreq call -f rCRS.fa --min-bq ${quality} --min-alt-bq ${quality} -o ${sampleID}.${method}.Q${quality}.vcf ${filtered_chrM}
    """
}

process merge_lofreq {
    tag "Merging Lofreq VCFs"

    input:
    path(vcfs) from lofreq_ch.collect()

    output:
    path("lofreq.txt") into lofreq_merged_ch

    script:
    """
    cp $baseDir/scripts/mergeVCFs.R . 
    Rscript mergeVCFs.R ${vcfs} lofreq.txt
    """
}

/////////////////
//// VarDict ////
/////////////////
process vardict {
    tag "Running VarDict"

    input:
    set sampleID, method, path(filtered_chrM) from merged_ch8
    each quality from base_qualities

    output:
    path("${sampleID}.${method}.Q${quality}.txt") into vardict_ch

    script:
    """
    samtools index ${filtered_chrM}
    cp $baseDir/data/rCRS.fa $baseDir/data/rCRS.fa.fai $baseDir/data/rCRS.bed .
    vardict-java -G rCRS.fa -f 0 -N ${sampleID} -b ${filtered_chrM} -c 1 -S 2 -E 3 rCRS.bed -q ${quality} > ${sampleID}.${method}.Q${quality}.txt
    """
}

process merge_vardict {
    tag "Merging VarDict VCFs"

    input:
    path(vcfs) from vardict_ch.collect()

    output:
    path("vardict.txt") into vardict_merged_ch

    script:
    """
    cp $baseDir/scripts/mergeCalls.R . 
    Rscript mergeCalls.R ${vcfs} vardict.txt
    """
}


/////////////////////////////
//// Sort out count data ////
/////////////////////////////
noneCounts_ch
    .mix(rtnCounts_ch)
    .mix(mapqCounts_ch)
    .mix(allCounts_ch)
    .set{ merged_ch10 }

/*
 * Filter the variant calls
 */
process merge_counts {
    tag "Merging read count data"

    input:
    path(counts) from merged_ch10.collect()

    output:
    path("counts.txt") into counts_ch

    script:
    """
    cp $baseDir/scripts/mergeCounts.R .
    Rscript mergeCounts.R ${counts}
    """
}

/*
 * Get specificity/sensitivity metrics
 */
process benchmark {
    tag "Running benchmarking"
    publishDir "${baseDir}" , mode:'copy'

    input:
    path(varscan) from varscan_merged_ch
    path(mutserve) from mutserve_merged_ch
    path(mutect) from mutect_merged_ch
    path(freebayes) from freebayes_merged_ch
    path(lofreq) from lofreq_merged_ch
    path(vardict) from vardict_merged_ch
    path(shifted_varscan) from shifted_varscan_merged_ch
    path(counts) from counts_ch
    
    output:
    //file("benchmark.html")

    script:
    """
    cp $baseDir/data/synthGoldStandard.txt .
    cp $baseDir/scripts/benchmark.Rmd .
    """
}

    // Rscript -e "rmarkdown::render('benchmark.Rmd', output_dir='files/')"

    // cp files/benchmark.html .

workflow.onComplete {
	log.info ( workflow.success ? "\nDone! All benchmarking complete.\n" : "\nOops .. something went wrong.\n" )
}